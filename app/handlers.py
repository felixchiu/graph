#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Author: Felix Chiu
#
import sys
if not '..' in sys.path: sys.path.append('..')

import os
import mimetypes
import json
from datetime import datetime
import time
import webapp2
import requests
from jinja2 import Template
#
# load singleton Utility and SystemConfig classes
#
from util.utility import Utility
from sysconf.config import SystemConfig
util = Utility()
config = SystemConfig()

class StaticFileHandler(webapp2.RequestHandler):

    static_filePath = None
    template_path = None
    list_dir = None
    expiry_timer = 600

    def get(self, path=None):
        #
        # Home page must be either "/" or "/index.html"
        # Do not allow direct browsing of "/static/index.html"
        #
        if path == 'index.html':
            self.response.set_status(404)
            return
        #
        # When path is None, it means home page
        #
        if path is None: path = 'index.html'
        #
        # Return static file content
        #
        abs_path = os.path.abspath(os.path.join(self.get_static_path(), path))
        #
        # Make sure it is browsing within the protected static file directory
        #
        parent_path = os.getcwd()
        if abs_path.find(parent_path) != 0:
            util.send_error(403, self.request, self.response, 'Forbidden')
            return
        #
        # Support directory listing?
        #
        if os.path.isdir(abs_path):
            if self.list_dir_allowed():
                #
                # Send directory listing
                #
                dir = os.listdir(abs_path)

                files = []
                for d in dir:
                    # ignore hidden files
                    if d.startswith('.'): continue
                    if d == 'Thumbs.db': continue

                    kv = { 'href': os.path.join('/static/'+path, d), 'desc' : d}
                    f = os.path.join(abs_path, d)
                    f_time = os.path.getmtime(f)
                    kv['modified'] = datetime.utcfromtimestamp(f_time).strftime('%a, %d %b %Y %X')

                    if os.path.isdir(f):
                        kv['size'] = ''
                    else:
                        kv['size'] = "{:,}".format(os.path.getsize(f))
                    files.append(kv)

                try:
                    with open(os.path.join(self.get_template_path(), 'dir.html'), 'r') as f:
                        template = Template(f.read())
                        s = template.render(title='Directory: '+path, path=path, not_empty=len(files) > 0, list=files)
                        self.response.headers['Content-Type'] = 'text/html'
                        self.response.out.write(s.decode('utf-8'))

                except Exception as e:
                    util.send_error(403, self.request, self.response, 'Forbidden')

            else:
                util.send_error(403, self.request, self.response, 'Forbidden')

            return

        try:
            with open(abs_path, 'r') as f:
                ts = datetime.utcfromtimestamp(time.time() + self.expiry_timer).strftime('%a, %d %b %Y %X')
                #
                # Set content type and enable HTTP content caching
                #
                self.response.headers['Content-Type'] = mimetypes.guess_type(abs_path)[0]
                self.response.headers['Cache-Control'] = 'max-age='+str(self.expiry_timer)+', must-revalidate'
                self.response.headers['Expires'] = ts+' GMT'
                #
                # Do streaming read to minimize memory footprint
                #
                while True:
                    b = f.read(1024)
                    if len(b) == 0: break
                    self.response.out.write(b)

        except:
            util.send_error(404, self.request, self.response, 'The resource could not be found.')

    def get_static_path(self):
        if self.static_filePath is None:
            p = config.get_item('server.staticFilePath')
            self.static_filePath = p if p is not None else 'static/'

        return self.static_filePath

    def get_template_path(self):
        if self.template_path is None:
            p = config.get_item('server.templatePath')
            self.template_path = p if p is not None else 'templates/'

        return self.template_path

    def list_dir_allowed(self):
        if self.list_dir is None:
            self.list_dir = config.get_item('server.listDirAllowed')

        return self.list_dir


class HelloHandler(webapp2.RequestHandler):

    def just_demo(self, year, day):
        query = util.parse_query(self.request.query_string)

        print "arguments =", year, day

        self.response.content_type = 'application/json'

        if 'statement' in query:
            h = {}
            h['content-type'] = 'application/json'
            s = {}
            statements = []
            statement = {}
            statement['statement'] = query['statement']
            statements.append(statement)
            s['statements'] = statements
            r = requests.post('http://127.0.0.1:7474/db/data/transaction/commit', data=json.dumps(s), headers=h)
            print r
            j = json.loads(r.text)
            self.response.out.write(json.dumps(j, sort_keys=True, indent=4))

        else:
            self.response.out.write('{"query": "%s"}' % query)
