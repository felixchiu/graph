#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Author: Felix Chiu
#
import sys
if not '..' in sys.path:
    sys.path.append('..')

import webapp2
import json
import requests
import time
from util.logger import LoggingService
#
# Initialize logging service
#
log = LoggingService('graph').getLogger()
#
# load singleton Utility and SystemConfig classes
#
from util.utility import Utility
from sysconf.config import SystemConfig
util = Utility()
config = SystemConfig()
h = {'content-type': 'application/json'}

neo4j = 'http://'+config.get_item('neo4j.host')+':'+str(config.get_item('neo4j.port'))
es = 'http://'+config.get_item('elasticSearch.host')+':'+str(config.get_item('elasticSearch.port'))

neo4j_commit = neo4j+'/db/data/transaction/commit'

__author__ = 'felixchiu'


class NodeHandler(webapp2.RequestHandler):

    def list_node_name(self):
        statement = 'MATCH a RETURN DISTINCT a.node as node, COUNT(*) AS count'
        ps = util.prepare_neo4j_statement(statement)
        r = requests.post(neo4j_commit, data=json.dumps(ps), headers=h)
        if r.status_code == 200:
            resp = json.loads(r.text)
            user_data = resp['results'][0]['data']
            if user_data is not None:
                packed_user_data = []
                size = len(resp['results'][0]['data'])
                for i in range(size):

                    item = {'node': resp['results'][0]['data'][i]['row'][0],
                            'count': resp['results'][0]['data'][i]['row'][1]}
                    packed_user_data.append(item)

                status_code = 200
                message = 'Node found'

                if size == 0:
                    status_code = 404
                    message = 'Node not found'

                result = {'code': status_code, 'message': message, 'result': packed_user_data}

                self.response.content_type = 'application/json'
                self.response.out.write(json.dumps(result, sort_keys=True, indent=4))
                return
        util.send_error(404, self.response, 'Node not found')


    def list_all_node_detail(self):
        self.list_node_worker('')

    def list_node(self, node_name):
        self.list_node_worker(node_name)

    def list_node_worker(self, node_name):
        if node_name == '':
            statement = 'MATCH d RETURN d ORDER BY d.node, d.name'
        else:
            statement = 'match (d:'+node_name+') return d'
        ps = util.prepare_neo4j_statement(statement)
        
        r = requests.post(neo4j_commit, data=json.dumps(ps), headers=h)

        if r.status_code == 200:
            resp = json.loads(r.text)
            user_data = resp['results'][0]['data']
            if user_data is not None:
                packed_user_data = []
                size = len(resp['results'][0]['data'])
                for i in range(size):
                    packed_user_data.append(resp['results'][0]['data'][i]['row'][0])

                status_code = 200
                message = 'Node found'

                if size == 0:
                    status_code = 404
                    message = 'No Node found'

                result = {'code': status_code, 'message': message, 'result': packed_user_data}

                self.response.content_type = 'application/json'
                self.response.out.write(json.dumps(result, sort_keys=True, indent=4))
                return
        util.send_error(404, self.response, 'Node '+node_name+' not found.')

    def get_node(self, uuid):
        #
        # Construct neo4j Cypher statement
        #
        statement = 'match (d) where d.uuid = "'+uuid+'" return d'
        ps = util.prepare_neo4j_statement(statement)

        
        r = requests.post(neo4j_commit, data=json.dumps(ps), headers=h)

        if r.status_code == 200:
            resp = json.loads(r.text)
            user_data = util.getMapElement('results[0].data[0].row[0]', resp)
            if user_data is not None:
                result = {'code': 200, 'message': 'Node found', 'user': user_data}
                self.response.content_type = 'application/json'
                self.response.out.write(json.dumps(result, sort_keys=True, indent=4))
                return

        util.send_error(404, self.response, 'Node '+uuid+' not found.')

    def create_node(self):

        o = json.loads(self.request.body)

        if 'node' not in o:
            util.send_error(400, self.response, 'Missing parameter (node)')
            return
        t = time.time()

        # Create Node Name (vertex name)
        node = o['node']
        node.replace(" ", "_")

        uuid = util.get_uuid()
        created = util.time_to_GMT(t)
        modified = util.time_to_GMT(t)
        #
        # Construct neo4j Cypher statement
        #
        statement = 'create (d:'+node+' {uuid: "'+uuid+'", '
        for key in o.keys():
            key.replace(" ", "_")
            statement += key + ':"' + o[key] + '", '
        statement += ' created: "'+created+'", modified: "'+modified+'"'
        statement += '}) return d'
        ps = util.prepare_neo4j_statement(statement)

        
        r = requests.post(neo4j_commit, data=json.dumps(ps), headers=h)

        if r.status_code == 200:
            resp = json.loads(r.text)
            user_data = util.getMapElement('results[0].data[0].row[0]', resp)
            if user_data is not None:
                result = {'code': 200, 'message': 'Node '+node+' created successfully', 'user': user_data}
                self.response.content_type = 'application/json'
                self.response.out.write(json.dumps(result, sort_keys=True, indent=4))
                log.info('Node '+node+' created: '+json.dumps(user_data))
                return

        util.send_error(400, self.response, 'Unable to create ' + node)

    def create_relationship(self, uuid1, uuid2):

        o = json.loads(self.request.body)

        if 'relation_type' not in o:
            util.send_error(400, self.response, 'Missing parameter (relation_type)')
            return
        t = time.time()

        # Create Node Name (vertex name)
        relation_type = o['relation_type']
        relation_type = relation_type.replace(" ", "_")

        uuid = util.get_uuid()
        created = util.time_to_GMT(t)
        modified = util.time_to_GMT(t)
        #
        # Construct neo4j Cypher statement
        #
        statement = 'MATCH (a) where a.uuid = "'+uuid1+'" MATCH (b) WHERE b.uuid = "'+uuid2 + '" '
        statement += 'CREATE (a)-[r:'+relation_type+' {uuid: "'+uuid+'", '
        for key in o.keys():
            key.replace(" ", "_")
            statement += key + ':"' + o[key] + '", '
        statement += ' created: "'+created+'", modified: "'+modified+'"'
        statement += '}]->(b) '
        statement += 'return r'
        ps = util.prepare_neo4j_statement(statement)

        
        r = requests.post(neo4j_commit, data=json.dumps(ps), headers=h)

        if r.status_code == 200:
            resp = json.loads(r.text)
            user_data = util.getMapElement('results[0].data[0].row[0]', resp)
            if user_data is not None:
                result = {'code': 200, 'message': 'Relationship '+relation_type+' created successfully',
                          'user': user_data}
                self.response.content_type = 'application/json'
                self.response.out.write(json.dumps(result, sort_keys=True, indent=4))
                log.info('Relationship '+relation_type+' created: '+json.dumps(user_data))
                return

        util.send_error(400, self.response, 'Unable to create relationship ' + relation_type)