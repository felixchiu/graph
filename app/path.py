#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Author: Felix Chiu
#
import sys
if not '..' in sys.path:
    sys.path.append('..')

import webapp2
import json
import requests
import time
from util.logger import LoggingService
#
# Initialize logging service
#
log = LoggingService('graph').getLogger()
#
# load singleton Utility and SystemConfig classes
#
from util.utility import Utility
from sysconf.config import SystemConfig
util = Utility()
config = SystemConfig()
h = {'content-type': 'application/json'}

neo4j = 'http://'+config.get_item('neo4j.host')+':'+str(config.get_item('neo4j.port'))
es = 'http://'+config.get_item('elasticSearch.host')+':'+str(config.get_item('elasticSearch.port'))

neo4j_commit = neo4j+'/db/data/transaction/commit'

__author__ = 'felixchiu'


class PathHandler(webapp2.RequestHandler):
    def get_path(self, position, uuid):
        statement = 'match (from)-[r]->(to) where from.uuid="' + uuid + '" return r, to'
        if position == 'inward':
            statement = 'match (from)-[r]->(to) where to.uuid="' + uuid + '" return r, from'
        ps = util.prepare_neo4j_statement(statement)
        r = requests.post(neo4j_commit, data=json.dumps(ps), headers=h)

        if r.status_code == 200:

            resp = json.loads(r.text)
            user_data = resp['results'][0]['data']
            if user_data is not None:
                packed_user_data = []
                size = len(resp['results'][0]['data'])
                for i in range(size):
                    e = {'r': resp['results'][0]['data'][i]['row'][0], 'n': resp['results'][0]['data'][i]['row'][1]}
                    packed_user_data.append(e)

                status_code = 200
                message = 'Relationship found'

                if size == 0:
                    status_code = 404
                    message = 'No relationship found for ' + position + ' with ' + uuid

                result = {'code': status_code, 'message': message, 'result': packed_user_data}

                self.response.content_type = 'application/json'
                self.response.out.write(json.dumps(result, sort_keys=True, indent=4))
                return
        util.send_error(404, self.response, 'No record found in ' + uuid + ' for ' + position)