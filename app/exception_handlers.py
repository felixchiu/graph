#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Author: Felix Chiu
#

from util.utility import Utility

class CustomExceptions(object):

    def __init__(self):
        self.util = Utility()

    def handle_404(self, request, response, exception):
        self.util.send_error(404, response, exception)

    def handle_500(self, request, response, exception):
        self.util.send_error(500, response, exception)