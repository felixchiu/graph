class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    Limitations: The decorated class cannot be inherited from.

    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    Enhanced from answer#81 of
    http://stackoverflow.com/questions/42558/python-and-the-singleton-pattern

    -   Added parameter passing at first instantiation - note that 2nd instantiation
        does not call the target class __init__() method
    -   The target class can be "instantiated" many times to return the same instance.
    -   Use Python 3 syntax to call a parent class constructor:
        super(self.__class__, self).__init__()

    Example:

    TO DECLARE A SINGLETON CLASS

    from decorators.practice import Singleton

    @Singleton
    class SystemConfig(BaseConfig):

        def __init__(self, *args, **kwargs):
            print "got some parameters", *args
            print "got some keyword parameters", **kwargs
            #
            # call the parent constructor if needed
            #
            super(self.__class__, self).__init__(*args, **kwargs)
            #

    TO "INSTANTIATE" A SINGLETON CLASS

    from sysconf.config import SystemConfig

    config = SystemConfig('hello world')
    another_config = SystemConfig('parameter to be ignored in 2nd instantiation')
    print "same instance of SystemConfig?", config = another_config

    """

    _instance = None

    def __init__(self, decorated):
        self._decorated = decorated

    def __call__(self, *args, **kwargs):
        if self._instance is None: self._instance = self._decorated(*args, **kwargs)
        return self._instance
