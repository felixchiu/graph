import os
import json
import requests

__author__ = 'Felix Chiu'

h = {'Content-Type': 'application/json'}
rel = {'relation_type': 'UNDER', 'category': 'File System'}


def create_file(current_full_path, filename, parent_uuid):
    if not filename.startswith("."):
        s = {'node': 'File', 'name': filename, 'path':current_full_path}
        if os.path.isdir(current_full_path):
            s = {'node': 'Directory', 'name': filename, 'path':current_full_path}
        r = requests.post('http://127.0.0.1:8088/n', headers=h, data=json.dumps(s))
        print r
        print r.text
        a = json.loads(r.text.encode("utf-8"))
        current_uuid = a['user']['uuid'].encode("utf-8")
        if parent_uuid != '':
            r = requests.post('http://127.0.0.1:8088/r/'+current_uuid+'/'+parent_uuid, headers=h, data=json.dumps(rel))
            print r
            print r.text
        if os.path.isdir(current_full_path):
            for f in os.listdir(current_full_path):
                create_file(os.path.join(current_full_path, f), f, current_uuid)

