from webapp2 import Route

from app.handlers import StaticFileHandler

rest_endpoints = [
    Route('/', handler=StaticFileHandler),
    Route('/index.html', handler=StaticFileHandler),
    Route(r'/static/<path:.+>', handler=StaticFileHandler),

    Route('/hello/<year>/<day>', handler='app.handlers.HelloHandler', handler_method='just_demo', methods=['GET']),
    #
    # For security, the /accounts should be restricted to admin users by the API gateway.
    # It is not reasonable to release this API to normal client applications
    # because only privileged admin users can create account (POST) and list "all" accounts (GET)
    #

    Route('/sn/<node_name>', handler='app.search.SearchHandler', handler_method='search_node', methods=['POST']),
    Route('/path/<position>/<uuid>', handler='app.path.PathHandler', handler_method='get_path', methods=['GET']),
    Route('/count', handler='app.node.NodeHandler', handler_method='list_node_name', methods=['GET']),
    Route('/n', handler='app.node.NodeHandler', handler_method='create_node', methods=['POST']),
    Route('/n/<uuid>', handler='app.node.NodeHandler', handler_method='get_node', methods=['GET']),
    Route('/l', handler='app.node.NodeHandler', handler_method='list_all_node_detail', methods=['GET']),
    Route('/r/<uuid1>/<uuid2>', handler='app.node.NodeHandler', handler_method='create_relationship', methods=['POST']),
    Route('/l/<node_name>', handler='app.node.NodeHandler', handler_method='list_node', methods=['GET']),
    # Add a button
]
