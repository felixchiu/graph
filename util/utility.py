import sys
if not '..' in sys.path: sys.path.append('..')

import os
import string
import random
import urllib
from urlparse import parse_qs
from datetime import datetime
import time
import uuid
import hashlib
import socket
import json
from lib.rfc3339 import timestamp_from_tf
from lib.rfc3339 import tf_from_timestamp

from decorators.practice import Singleton

@Singleton
class Utility(object):
    """ simple utility for crypto and misc functions """

    def __init__(self):
        uc = [chr(x+65) for x in xrange(26)]
        lc = [chr(x+97) for x in xrange(26)]
        n = [str(x) for x in xrange(10)]
        n.extend(uc)
        n.extend(lc)
        self._b62chars = n

    def socket_ready(self, host, port):
        s = None
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, port))
            return True
        except socket.error:
            return False
        finally:
            if s is not None: s.close()

    def send_error(self, code, response, message):
        o = {}
        o['code'] = code
        o['message'] = str(message)
        response.set_status(code)
        response.content_type = 'application/json'
        response.out.write(json.dumps(o))

    def prepare_neo4j_statement(self, command):
        s = {}
        statement = {}
        statement['statement'] = command

        statements = []
        statements.append(statement)
        s['statements'] = statements
        return s

    def time_to_GMT(self, t):
        """
        This function turns epoch second time value into ISO3339 time string
        This is the time value to be stored in the graph and elastic search because it is sortable
        and portable among timezone and machines.

        :param t: epoch seconds (floating point)
        :return: ISO3339 time string in GMT
        """
        return datetime.utcfromtimestamp(t).strftime('%Y-%m-%dT%XZ')

    def time_to_ISO3339(self, t):
        """
        This function turns epoch second time value into ISO3339 time string
        for delivery to client applications

        :param t: epoch seconds (floating point)
        :return: ISO3339 time string with time zone offset
        """
        return timestamp_from_tf(t)

    def iso3339_to_time(self, ts):
        """
        This function turns ISO3339 time string into epoch seconds.
        This is usually the input format from client applications.

        :param ts: ISO3339 time string with time zone offset
        :return: epoch seconds with precision up to seconds
        """
        return tf_from_timestamp(ts)

    def getMapElement(self, path, map):
        #
        # convenient read element function copied from consul_lib
        # This is the easiest way to find an element from a complex map
        #
        if not path:
            return None
        if not map:
            return None
        if ('.' not in path) and ('.' not in path) and ('[' not in path):
            if path in map:
                return map[path]
            else:
                return None
        # support both dot and slash as separator
        path_list = self.splitPath(path.replace('/', '.') if '/' in path else path, '.')
        o = map
        length = len(path_list)
        n = 0
        for p in path_list:
            n += 1
            # list item?
            if '[' in p and p.endswith(']') and (not p.startswith('[')):
                start = p.index('[')
                end = p.rindex(']')
                if start > end: return None

                key = p[:start]
                index = p[start+1 : end].strip()
                if len(index) == 0: return None
                if not index.isdigit(): return None
                i = int(index)

                if key in o:
                    x = o[key]
                    if type(x) is list:
                        if i >= len(x): return None
                        if n == length: return x[i]
                        if type(x[i]) is dict:
                            o = x[i]
                            continue
                return None
            # target found
            if p in o:
                x = o[p]
                if n == length: return x
                if type(x) is dict:
                    o = x
                    continue
            return None
        return None

    def splitPath(self, path, c):
        #
        # convenient function copied from consul_lib
        #
        rv = []
        parts = path.split(c)
        for p in parts:
            if len(p) == 0:
                continue
            else:
                rv.append(p)
        return rv

    def generate_secret(self, length=16):
        """ generate secret using b62 dataset """
        return ''.join([str(random.choice(self._b62chars)) for i in xrange(length)])

    def generate_nonce(self, length=8):
        """Generate pseudorandom number."""
        return ''.join([str(random.randint(0, 9)) for i in range(length)])
        
    def file_to_str(self, path):
        f = open(path)
        s = f.read()
        f.close()
        return s
    
    def get_filehash(self, path):
        sha256 = hashlib.sha256()
        with open(path,'rb') as inputFile: 
            for chunk in iter(lambda: inputFile.read(4096), b''): 
                sha256.update(chunk)
        sha256.update(str(os.path.getsize(path))) # Update with file size
        return sha256.hexdigest()
        
    def safe_html(self, s):
        return s.replace('>', '&gt;').replace('<', '&lt;')
        
    def parse_query(self, param_str):
        """Turn URL string into parameters."""
        parameters = parse_qs(param_str.encode('utf-8'), keep_blank_values=True)
        for k, v in parameters.iteritems():
            parameters[k] = urllib.unquote(v[0])
        return parameters
        
    def isGoodPassword(self, pwd):
        has_upper = False
        has_lower = False
        has_number = False
        has_special = False
        for i in pwd:
            if 'a' <= i and i <= 'z':
                has_lower = True
            elif 'A' <= i and i <= 'Z':
                has_upper = True
            elif '0' <= i and i <= '9':
                has_number = True
            else: 
                has_special = True
        n = 0
        if has_upper: n += 1
        if has_lower: n += 1
        if has_number: n += 1
        if has_special: n += 1
        return { 'points': n, 'upper': has_upper, 'lower': has_lower, 'special': has_special }    
    
    def str2int(self, s):
        value = 0
        try:
            value = int(s)
            return value
        except ValueError:
            return -1
        
    def date2ms(self, dt):
        " convert datetime to milliseconds with precision to seconds "
        seconds = time.mktime(dt.timetuple())
        return int(seconds * 1000)
    
    def ms2date(self, t):
        " milliseconds to datetime with precision to seconds "
        return datetime.fromtimestamp(int(t/1000))
    
    def seconds2date(self, t):
        " seconds to datetime with precision to seconds "
        return datetime.fromtimestamp(int(t))
    
    def get_uuid(self):
        return str(uuid.uuid4())
    
    def escape(self, s):
        " Escape a URL including any /. "
        return urllib.quote(s, safe='~')
    
    def is_hash256(self, h):
        " check if the string has 64 hex digits "
        if len(h) != 64: return False
        return self.is_hex(h)
    
    def is_hex(self, s):
        " hex string must be even length in the range 0-9, a-f or A-F "
        if len(s) % 2: return False
        for c in s:
            if not (c in string.hexdigits): return False
        return True
    
