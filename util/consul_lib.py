import requests
import json
import base64
import hashlib
import urllib
import yaml

class Consul():

    http = None

    def __init__(self):
        self.http = requests.Session()
    
    def getKvFromConsul(self, path, recursive, last_modified, timeout_seconds):
        url = 'http://127.0.0.1:8500/v1/kv/'+path+'?wait='+str(timeout_seconds)+'s'
        if recursive:
            url += '&recurse'
        if last_modified > 0:
            url = url + '&index='+str(last_modified)
    
        r = None
        try:
            r = self.http.get(url, timeout=timeout_seconds-1 if timeout_seconds > 2 else timeout_seconds)
        except requests.exceptions.Timeout:
            # empty list to indicate timeout exception
            return [ ]
        if r is not None and r.status_code == 200:
            json_list = json.loads(r.text)
            #
            # Convert unicode json to UTF-8 dict
            #
            result = []
            if type(json_list) is list:
                for v in json_list:
                    if type(v) is dict:
                        rv = {}
                        for d in v:
                            s = v[d]
                            rv[str(d)] = str(s) if type(s) is unicode else s
                        result.append(rv)
            return result
        else:
            return None

    def putKvToConsul(self, path, value):
        url = 'http://127.0.0.1:8500/v1/kv/'+(path[1:] if path.startswith('/') else path)
        r = self.http.put(url, str(value))
        return True if r.status_code == 200 else False
        
    def delKvFromConsul(self, path):
        url = 'http://127.0.0.1:8500/v1/kv/'+(path[1:] if path.startswith('/') else path)
        r = self.http.delete(url)
        return True if r.status_code == 200 else False

    def putMapToConsul(self, path, source, timeout_seconds):
        for p in source:
            composite = p if path is None or path == '/' else path+"/"+p
            v = source[p]
            if type(v) is dict:
                self.putMapToConsul(composite, v, timeout_seconds)
            elif type(v) is list:
                self.putListToConsul(composite, v, timeout_seconds)
            else:
                self.updateKvToConsul(composite, v, timeout_seconds)
                
    def putListToConsul(self, path, source, timeout_seconds):
        n = 0
        for v in source:
            composite = path+urllib.quote('['+str(n)+']')
            n += 1
            if type(v) is dict:
                self.putMapToConsul(composite, v, timeout_seconds)
            elif type(v) is list:
                self.putListToConsul(composite, v, timeout_seconds)
            else:
                self.updateKvToConsul(composite, v, timeout_seconds)
                
    def updateKvToConsul(self, path, obj, timeout_seconds):
        # guarantee string value
        value = obj if type(obj) is str else str(obj)
        kv = self.getKvFromConsul(path, False, 0, timeout_seconds)
        if kv is None:
            self.putKvToConsul(path, value if value is not None else 'null')
            return
            
        same_value = False
        for x in kv:
            if 'Key' in x and 'Value' in x:
                k = x['Key']
                v = x['Value']
                if k.endswith('/'): continue
                v = base64.b64decode(v) if v else ''
                if v == value: same_value = True
                break
        if not same_value:
            self.putKvToConsul(path, value if value is not None else 'null')
            
    def getKeys(self, path):
        url = 'http://127.0.0.1:8500/v1/kv/'+path+'?keys'
        r = self.http.get(url)
        if r.status_code == 200:
            return json.loads(r.text)
        else:
            return None
            
    def getLastModified(self, kv):
        data = {} if kv is None else kv
        index = 0
        index_list = []
        for x in data:
            if 'ModifyIndex' in x:
                i = x['ModifyIndex']
                index_list.append(i)
                if i > index: index = i
        if len(index_list) > 1: index_list.sort()
        h = hashlib.sha1()
        h.update(str(len(index_list)))
        for v in index_list: h.update(str(v))
        return [index, h.hexdigest()]

    def normalizeMap(self, consul_path, kv):
        if kv is None: return {}
        [keys, map] = self.getTree(consul_path, kv)
        
        result = {}
        lists = {}
        unique_paths = []
        #
        # Count the max depth
        #
        depth = 0
        for s in map:
            path = self.splitPath(s, '.')
            if len(path) > depth: depth = len(path)
            
        if depth == 0: return result
        #
        # Scan each level and normalize list indexes to absolute values
        #
        for i in range(1, depth+1):
            for s in keys:
                path = self.splitPath(s, '.')
                pathname = ''
                j=0
                while j < i and j < len(path):
                    pathname += path[j]
                    pathname += '.'
                    j += 1
                normalized_path = pathname[:len(pathname)-1]
                if normalized_path not in unique_paths:
                    unique_paths.append(normalized_path)
                    
                    element = self.getLastElement(normalized_path)
                    if self.isList(element):
                        normalized_key = normalized_path[: normalized_path.rindex('[')]
                        if normalized_key not in lists:
                            lists[normalized_key] = 1
                            lists[normalized_path] = 0
                        else:
                            count = lists[normalized_key]
                            lists[normalized_key] = count + 1
                            lists[normalized_path] = count
        
        for s in keys:
            self.createNode(self.getAbsolutePath(s, lists), map[s], result)
        return result
        
    def getTree(self, path, kv):
        # extract key-value from kv as a sorted dict
        prefix = path.replace('/', '.')
        if len(prefix) > 1:
            if prefix.startswith('.'): prefix = prefix[1:]
            if not prefix.endswith('.'): prefix += '.'
        o = {}
        keys = []
        for x in kv:
            if 'Key' in x and 'Value' in x:
                k = x['Key']
                v = x['Value']
                if k.endswith('/'): continue
                k = k.replace('/', '.')
                v = base64.b64decode(v) if v else ''
                if prefix == '.':
                    o[k] = v
                    keys.append(k)
                elif k.startswith(prefix):
                    o[k[len(prefix):]] = v
                    keys.append(k[len(prefix):])
        # sort the keys
        return [sorted(keys), o]
        
    def createNode(self, absolute_path, value, source):
        # create / update node to the source dict
        absPath = absolute_path.replace('/', '.') if '/' in absolute_path else absolute_path
        path = self.splitPath(absPath, '.')        
        size = len(path)
        element = path[len(path) -1]
        current_path = absPath
        parent_path = self.getParentPath(absPath)
        current = self.getMapElement(current_path, source)
        if current is not None:
            if self.isList(element):
                parent = current_path[: current_path.rindex('[')]
                n = self.getIndex(element)
                cp = self.getMapElement(parent, source)
                if type(cp) is list:
                    cp[n] = self.getObject(value)
            else:
                if parent_path is None:
                    source[element] = self.getObject(value)
                else:
                    o = self.getMapElement(parent_path, source)
                    if type(o) is dict:
                        o[element] = self.getObject(value)
        else:
            if size == 1:
                if self.isList(element):
                    list_element = element[: element.rindex('[')]
                    parent = current_path[: current_path.rindex('[')]
                    n = self.getIndex(element)
                    cp = self.getMapElement(parent, source)
                    if cp is None:
                        nlist = []
                        if n > 0:
                            for i in range(n):
                                nlist.append(None)
                        nlist.append(self.getObject(value))
                        source[list_element] = nlist
                    elif type(cp) is list:
                        if len(cp) > n:
                            cp[n] = self.getObject(value)
                        else:
                            while n > len(cp):
                                cp.append(None)
                            cp.append(self.getObject(value))
                else:
                    source[element] = self.getObject(value)
            else:
                if self.isList(element):
                    list_element = element[: element.rindex('[')]
                    parent = current_path[: current_path.rindex('[')]
                    n = self.getIndex(element)
                    cp = self.getMapElement(parent, source)
                    if cp is None:
                        nlist = []
                        if n > 0:
                            for i in range(n):
                                nlist.append(None)
                        nlist.append(self.getObject(value))
                        o = self.getMapElement(parent_path, source)
                        if type(o) is dict:
                            o[list_element] = nlist
                        else:
                            self.createNode(parent_path, {list_element: nlist}, source)
                    elif type(cp) is list:
                        if len(cp) > n:
                            cp[n] = self.getObject(value)
                        else:
                            while n > len(cp):
                                cp.append(None)
                            cp.append(self.getObject(value))
                else:
                    o = self.getMapElement(parent_path, source)
                    if type(o) is dict:
                        o[element] = self.getObject(value)
                    else:
                        self.createNode(parent_path, {element: self.getObject(value)}, source)                
        
    def getObject(self, v):
        # return correct object type
        if type(v) is str:
            lv = v.lower()
            if lv == 'true': return True
            if lv == 'false': return False
            if lv == 'null': return None
            if len(lv) == 0: return ''
            if lv.startswith('-') and lv[1:].isdigit(): return int(lv[1:]) * -1
            if lv.isdigit(): return int(lv)
        return v
    
    def splitPath(self, path, c):
        rv = []
        parts = path.split(c)
        for p in parts:
            if len(p) == 0:
                continue
            else:
                rv.append(p)
        return rv
        
    def getParentPath(self, path):
        return path[: path.rindex('.')] if '.' in path else None
            
    def getLastElement(self, path):
        return path[path.rindex('.'):] if '.' in path else path
            
    def getAbsolutePath(self, pathname, lists):
        relative_path = ''
        absolute_path = ''
        path = self.splitPath(pathname, '.')
        for p in path:
            relative_path += p
            if self.isList(p):
                n = lists[relative_path]
                absolute_path += p[:p.index('[')]
                absolute_path += ('[' + str(n) + ']')
            else:
                absolute_path += p
            relative_path += '.'
            absolute_path += '.'
        return absolute_path[: len(absolute_path)-1]
            
    def isList(self, p):
        return '[' in p and p.endswith(']') and (not p.startswith('['))
        
    def getIndex(self, p):
        start = p.rindex('[')
        end = p.rindex(']')
        if start > end: return None
        index = p[start+1 : end].strip()
        if len(index) == 0: return None
        if not index.isdigit(): return None
        return int(index)
            
    def getMapElement(self, path, map):
        if not path: return None
        if not map: return None
        if ('.' not in path) and ('.' not in path) and ('[' not in path):
            if path in map:
                return map[path]
            else:
                return None
        # support both dot and slash as separator
        path_list = self.splitPath(path.replace('/', '.') if '/' in path else path, '.')
        o = map
        length = len(path_list)
        n = 0
        for p in path_list:
            n += 1
            # list item?
            if '[' in p and p.endswith(']') and (not p.startswith('[')):
                start = p.index('[')
                end = p.rindex(']')
                if start > end: return None
                
                key = p[:start]
                index = p[start+1 : end].strip()
                if len(index) == 0: return None
                if not index.isdigit(): return None
                i = int(index)
                
                if key in o:
                    x = o[key]
                    if type(x) is list:
                        if i >= len(x): return None
                        if n == length: return x[i]
                        if type(x[i]) is dict:
                            o = x[i]
                            continue
                return None
            # target found
            if p in o:
                x = o[p]
                if n == length: return x
                if type(x) is dict:
                    o = x
                    continue
            return None
        return None
        
    def mergeMaps(self, path, source, target):
        if type(source) is dict and type(target) is dict:
            for p in source:
                composite = p if path is None or path == '/' else path+"/"+p
                v = source[p]
                if type(v) is dict:
                    self.mergeMaps(composite, v, target)
                elif type(v) is list:
                    self.mergeList2Map(composite, v, target)
                else:
                    self.createNode(composite, v, target)
                
    def mergeList2Map(self, path, source, target):
        if type(source) is list and type(target) is dict:
            n = 0
            for v in source:
                composite = path+'['+str(n)+']'
                n += 1
                if type(v) is dict:
                    self.mergeMaps(composite, v, target)
                elif type(v) is list:
                    self.mergeList2Map(composite, v, target)
                else:
                    self.createNode(composite, v, target)
        
    def map2yaml(self, map, comment=''):
        return "--- # " + comment + "\n\n" + yaml.dump(map, default_flow_style=False)
        
    def yaml2map(self, data):
        # data can be string or file object
        return yaml.load(data)
        
        
        