import sys
if not '..' in sys.path: sys.path.append('..')

from decorators.practice import Singleton
import logging

@Singleton
class LoggingService(object):

    logger = None
    log_name = None

    def __init__(self, *args, **kwargs):
        self.log_name = 'unknown' if (len(args) == 0) else args[0]
        self.logger = logging.getLogger(self.log_name)
        self.logger.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(filename)s:%(lineno)s %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

        """
        fh = logging.FileHandler(log_name+'.log')
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        """

    def getLogger(self):
        return self.logger

    def getName(self):
        return self.log_name