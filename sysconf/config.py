import sys
if not '..' in sys.path: sys.path.append('..')

import os
import socket

from decorators.practice import Singleton
from util.logger import LoggingService
#
# Initialize logging service
#
log = LoggingService('graph').getLogger()
#
# load singleton utility class
#
from util.utility import Utility
util = Utility()

from util.consul_lib import Consul
consul = Consul()

@Singleton
class SystemConfig(object):
    """ singleton for loading system configuration parameter """

    config_map = None
    env_map = None
    #
    # OK - this is a hardcoded application name
    #
    app_name = 'graph'
    timeout = 10 # seconds

    def load(self):
        env_path = 'config/env.yaml'
        global_path = 'config/global.yaml'
        server_path = 'config/server.yaml'
        if not os.path.exists(env_path):
            log.error(env_path+' not found')
            raise IOError(404, env_path+' not found')
        if not os.path.isfile(env_path):
            log.error(env_path+' is not a file')
            raise IOError(404, env_path+' is not a file')

        if not os.path.exists(global_path):
            log.error(global_path+' not found')
            raise IOError(404, global_path+' not found')
        if not os.path.isfile(global_path):
            log.error(global_path+' is not a file')
            raise IOError(404, global_path+' is not a file')

        if not os.path.exists(server_path):
            log.error(server_path+' not found')
            raise IOError(404, server_path+' not found')
        if not os.path.isfile(server_path):
            log.error(server_path+' is not a file')
            raise IOError(404, server_path+' is not a file')
        #
        # If consul is ready, override YAML with consul config
        #
        global_map = None
        server_map = None
        consul_ready = util.socket_ready('127.0.0.1', 8500)
        if consul_ready:
            log.info("Loading configuration from Consul")
            hostname = socket.gethostname().replace('/', '_')

            c_env_path = '/env'
            c_global_path = '/global/'+self.app_name
            c_server_path = '/server/'+hostname+'/'+self.app_name

            if consul.getKeys(c_env_path) is None:
                with open(env_path) as f:
                    map = consul.yaml2map(f)
                    consul.putMapToConsul(c_env_path, map, self.timeout)
                    log.info(c_env_path+" uploaded")
            if consul.getKeys(c_global_path) is None:
                with open(global_path) as f:
                    map = consul.yaml2map(f)
                    consul.putMapToConsul(c_global_path, map, self.timeout)
                    log.info(c_global_path+" uploaded")
            if consul.getKeys(c_server_path) is None:
                with open(server_path) as f:
                    map = consul.yaml2map(f)
                    consul.putMapToConsul(c_server_path, map, self.timeout)
                    log.info(c_server_path+" uploaded")

            kv = consul.getKvFromConsul(c_env_path, True, 0, self.timeout)
            if kv is not None:
                self.env_map = consul.normalizeMap(c_env_path, kv)
                log.info('Loaded '+c_env_path+' from Consul')
            else:
                raise Exception('Unable to download '+c_env_path+' from Consul')

            kv = consul.getKvFromConsul(c_global_path, True, 0, self.timeout)
            if kv is not None:
                global_map = consul.normalizeMap(c_global_path, kv)
                log.info('Loaded '+c_global_path+' from Consul')
            else:

                raise Exception('Unable to download '+c_global_path+' from Consul')

            kv = consul.getKvFromConsul(c_server_path, True, 0, self.timeout)
            if kv is not None:
                server_map = consul.normalizeMap(c_server_path, kv)
                log.info('Loaded '+c_server_path+' from Consul')
            else:
                raise Exception('Unable to download '+c_server_path+' from Consul')

        else:
            log.warn("Consul @127.0.0.1:8500 is not ready")
            log.info("Loading configuration from YAML files")
            with open(env_path) as f: self.env_map = consul.yaml2map(f)
            with open(global_path) as f: global_map = consul.yaml2map(f)
            with open(server_path) as f: server_map = consul.yaml2map(f)

        self.config_map = {}
        consul.mergeMaps('/', global_map, self.config_map)
        consul.mergeMaps('/', server_map, self.config_map)
        log.info("Configuration loaded")

    def get_item(self, path):
        if self.config_map is None:
            log.error('Configuration parameters not loaded')
            raise Exception('Configuration parameters not loaded')
        item = consul.getMapElement(path, self.config_map)

        if item is not None:
            s = str(item).strip()
            if s.startswith('${') and s.endswith('}'):
                mapped_item = consul.getMapElement(s[2:-1].strip(), self.env_map)
                if mapped_item is None:
                    log.warn('Parameter '+path+' '+item+' is not found in environment')
                return mapped_item

        if item is None:
            log.warn('Parameter '+path+' is not found')

        return item
