#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Author: Felix Chiu
#
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import webapp2
from paste import httpserver
from util.logger import LoggingService
from app.exception_handlers import CustomExceptions
from routes import rest_endpoints
from sysconf.config import SystemConfig
from util.utility import Utility
#
# Initialize logging service
#
log = LoggingService('graph').getLogger()
#
# Configure WSGI application
#
myApp = webapp2.WSGIApplication(rest_endpoints, debug=True)

def main():
    #
    # Load utility class
    #
    util = Utility()
    #
    # Load configuration from YAML or Consul automatically
    #
    config = SystemConfig()
    config.load()
    #
    # Check if Neo4j and Elastic Search are running
    #
    neo4j_host = config.get_item('neo4j.host')
    neo4j_port = config.get_item('neo4j.port')
    es_host = config.get_item('elasticSearch.host')
    es_port = config.get_item('elasticSearch.port')

    if neo4j_host is None:
        raise Exception('Neo4j.host is not configured')
    if type(neo4j_host) is not str:
        raise Exception('Neo4j.host is not a string')
    if neo4j_port is None:
        raise Exception('Neo4j.port is not configured')
    if type(neo4j_port) is not int:
        raise Exception('Neo4j.host is not an integer')

    if es_host is None:
        raise Exception('elasticSearch.host is not configured')
    if type(es_host) is not str:
        raise Exception('elasticSearch.host is not a string')
    if es_port is None:
        raise Exception('elasticSearch.port is not configured')
    if type(es_port) is not int:
        raise Exception('elasticSearch.host is not an integer')

    if util.socket_ready(neo4j_host, neo4j_port):
        log.info('Neo4j is configured as '+neo4j_host+':'+str(neo4j_port))
    else:
        log.warning('Neo4j is not running at '+neo4j_host+':'+str(neo4j_port))
        #raise Exception('Neo4j is not running at '+neo4j_host+':'+str(neo4j_port))

    if util.socket_ready(es_host, es_port):
        log.info('Elastic Search is configured as '+es_host+':'+str(es_port))
    else:
        log.warning('Elastic Search is not running at '+es_host+':'+str(es_port))
        #raise Exception('Elastic Search is not running at '+es_host+':'+str(es_port))

    port = config.get_item('server.port')
    if port is None:
        port = 8080

    #
    # Start server
    #
    log.info("Starting Graph Server @ "+str(port))
    httpserver.serve(myApp, host='0.0.0.0', port=port)

# custom error pages
custom_exceptions = CustomExceptions()
myApp.error_handlers[404] = custom_exceptions.handle_404
myApp.error_handlers[500] = custom_exceptions.handle_500

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        log.error(e)
